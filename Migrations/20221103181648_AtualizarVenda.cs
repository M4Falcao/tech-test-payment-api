﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace tech_test_payment_api.Migrations
{
    public partial class AtualizarVenda : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Itens_Vendas_VendaId",
                table: "Itens");

            migrationBuilder.DropIndex(
                name: "IX_Itens_VendaId",
                table: "Itens");

            migrationBuilder.DropColumn(
                name: "VendaId",
                table: "Itens");

            migrationBuilder.AddColumn<string>(
                name: "Itens",
                table: "Vendas",
                type: "nvarchar(max)",
                nullable: false);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Itens",
                table: "Vendas");

            migrationBuilder.AddColumn<int>(
                name: "VendaId",
                table: "Itens",
                type: "int",
                nullable: true);

            migrationBuilder.CreateIndex(
                name: "IX_Itens_VendaId",
                table: "Itens",
                column: "VendaId");

            migrationBuilder.AddForeignKey(
                name: "FK_Itens_Vendas_VendaId",
                table: "Itens",
                column: "VendaId",
                principalTable: "Vendas",
                principalColumn: "Id");
        }
    }
}
