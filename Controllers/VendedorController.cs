using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendedorController : ControllerBase
    {
        private readonly PagamentoContext _context;

        public VendedorController(PagamentoContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult Criar(Vendedor vendedor)
        {
            _context.Vendedores.Add(vendedor);
            _context.SaveChanges();

            return Ok(vendedor);
        }
        [HttpGet]
        public IActionResult Achar(int Id)
        {
            var vendedor = _context.Vendedores.Find(Id);
            if (vendedor == null) { return NotFound(); }

            return Ok(vendedor);
        }
        [HttpGet("Todos")]
        public IActionResult Todos()
        {
            var vendedor = _context.Vendedores.ToList();
            if (vendedor == null) { return NotFound(); }

            return Ok(vendedor);
        }

        [HttpPut]
        public IActionResult Atualizar(Vendedor vendedor)
        {
            var vendedorBase = _context.Vendedores.Find(vendedor.Id);
            if (vendedorBase == null) { return NotFound(); }

            vendedorBase.Cpf = vendedor.Cpf;
            vendedorBase.Email = vendedor.Email;
            vendedorBase.Nome = vendedor.Nome;
            vendedorBase.Telefone = vendedor.Telefone;

            _context.Vendedores.Update(vendedorBase);
            _context.SaveChanges();

            return Ok(vendedorBase);
        }
        [HttpDelete]
        public IActionResult Deletar(int Id)
        {
            var vendedor = _context.Vendedores.Find(Id);
            if (vendedor == null) { return NotFound(); }

            _context.Vendedores.Remove(vendedor);
            _context.SaveChanges();

            return Ok(vendedor);
        }

    }
}