using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using tech_test_payment_api.Context;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly PagamentoContext _context;

        public VendaController(PagamentoContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult Criar(Venda venda)
        {
            venda.Status = EnumStatusVenda.Aguardando_pagamento;
            _context.Vendas.Add(venda);
            _context.SaveChanges();

            return Ok(venda);
        }
        [HttpGet]
        public IActionResult Achar(int Id)
        {
            var venda = _context.Vendas.Find(Id);
            if (venda == null) { return NotFound(); }

            return Ok(venda);
        }

        [HttpGet("Todos")]
        public IActionResult Todos()
        {
            var venda = _context.Vendas.ToList();
            if (venda == null) { return NotFound(); }

            return Ok(venda);
        }

        [HttpPut]
        public IActionResult Atualizar(Venda venda)
        {
            var vendaBase = _context.Vendas.Find(venda.Id);
            if (vendaBase == null) { return NotFound(); }

            if (vendaBase.Status == EnumStatusVenda.Aguardando_pagamento)
            {
                if (venda.Status != EnumStatusVenda.Cancelada && venda.Status != EnumStatusVenda.Pagamento_aprovado)
                {
                    return BadRequest("Status invalido");
                }
            }
            if (vendaBase.Status == EnumStatusVenda.Pagamento_aprovado)
            {
                if (venda.Status != EnumStatusVenda.Cancelada && venda.Status != EnumStatusVenda.Enviado_para_transportadora)
                {
                    return BadRequest("Status invalido");
                }
            }
            if (vendaBase.Status == EnumStatusVenda.Enviado_para_transportadora)
            {
                if (venda.Status != EnumStatusVenda.Entregue)
                {
                    return BadRequest("Status invalido");
                }
            }
            if (vendaBase.Status == EnumStatusVenda.Cancelada || vendaBase.Status == EnumStatusVenda.Entregue)
            {
                return BadRequest("Status invalido");
            }

            vendaBase.Status = venda.Status;
            vendaBase.Itens = venda.Itens;
            vendaBase.Vendedor = venda.Vendedor;

            _context.Vendas.Update(vendaBase);
            _context.SaveChanges();

            return Ok(vendaBase);
        }
        [HttpDelete]
        public IActionResult Deletar(int Id)
        {
            var venda = _context.Vendas.Find(Id);
            if (venda == null) { return NotFound(); }

            _context.Vendas.Remove(venda);
            _context.SaveChanges();

            return NoContent();
        }
    }
}